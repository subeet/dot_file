#!/bin/zsh

#                               m""    "    ""#
#        mmmm    m mm   mmm   mm#mm  mmm      #     mmm
#        #" "#   #"  " #" "#    #      #      #    #"  #
#        #   #   #     #   #    #      #      #    #""""
#   #    ##m#"   #     "#m#"    #    mm#mm    "mm  "#mm"
#        #
#        "

#export PATH="$PATH:$(du "$HOME/.local/bin/" | cut -f2 | paste -sd ':')"
export PATH="$PATH:$HOME/.local/bin"
export PATH="$PATH:$HOME/.config/npm_global/bin"
export PATH="$PATH:$HOME/.gem/ruby/2.7.0/bin"
export PATH="$PATH:$HOME/.local/share/cargo/bin"
export PATH="$PATH:$HOME/.local/bin/luke_bar"
export PATH="$PATH:$HOME/.dotnet"


# Default programs:
export EDITOR="nvim"
export TERMINAL="st"
export BROWSER="brave"
export READER="zathura"
export FILE="ranger"

# ~/ Clean-up:
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_CACHE_HOME="$HOME/.cache"
export ZDOTDIR="${XDG_CONFIG_HOME:-$HOME/.config}/zsh"
export GOPATH="${XDG_DATA_HOME:-$HOME/.local/share}/go"

export LANG=en_US.UTF-8
export LC_CTYPE=en_US.UTF-8


if pacman -Qs libxft-bgra >/dev/null 2>&1; then
	# Start graphical server on tty1 if not already running.
	[ "$(tty)" = "/dev/tty1" ] && ! pidof Xorg >/dev/null 2>&1  && exec startx
else
	echo "\033[31mIMPORTANT\033[0m: Note that \033[32m\`libxft-bgra\`\033[0m must be installed for this build of dwm.
Please run:
	\033[32myay -S libxft-bgra\033[0m
and replace \`libxft\`"
fi

export PATH="/home/subeet/.local/share/cargo/bin:$PATH"

# Switch escape and caps if tty and no passwd required:
sudo -n loadkeys ${XDG_DATA_HOME:-$HOME/.local/share}/ttymaps.kmap 2>/dev/null
