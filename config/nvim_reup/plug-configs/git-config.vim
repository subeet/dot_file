" mappings

" Vim Fugitivie
nnoremap <leader>gs    :G<cr>
nnoremap <leader>gc    :Git commit<cr>
nnoremap <leader>grm   :GDelete<cr>


" Vim signify

" defautl value is 4000ms , not good for async update
set updatetime=100


" vim git gutter
nmap ]h                 <Plug>(GitGutterNextHunk)
nmap [h                 <Plug>(GitGutterPrevHunk)
nmap <leader>ghs        <Plug>(GitGutterStageHunk)
nmap <leader>ghu        <Plug>(GitGutterUndoHunk)
nmap <leader>ghp        <Plug>(GitGutterPreviewHunk)
