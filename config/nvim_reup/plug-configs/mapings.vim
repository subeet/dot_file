let mapleader=' '

nnoremap <leader>/ :noh<CR>

vmap < <gv
vmap > >gv


" Changing currenct working directory
nnoremap <leader>cd :cd %:p:h<cr>:pwd<cr>

vnoremap J :m '>+1<CR>gv=gv
vnoremap K :m '<-2<CR>gv=gv
vnoremap X "_d


"------------------------ Autocmd ------------------

" Delete white trailing spaces
autocmd BufWritePre * %s/\s\+$//e

" for compiling c++
autocmd filetype cpp map <F8> :!g++ -std=c++17 -Wshadow -Wall -g -o program "%:r.cpp" -O2 -Wno-unused-result -DLOCAL<CR>
autocmd filetype cpp map <F9> :!st -e bash -c '"%:p:h/program" ; read'<CR>

" Run xrdb whenever Xdefaults or Xresources are updated.
autocmd BufWritePost *Xresources,*Xdefaults !xrdb %

" Update binds when sxhkdrc is updated.
autocmd BufWritePost *sxhkdrc !pkill -USR1 sxhkd

" Auto Format javascript file
autocmd BufWritePost *.js lua vim.lsp.buf.formatting()

