let g:fzf_action = {
	\ 'ctrl-t': 'tab-split',
	\ 'ctrl-x': 'split',
	\ 'ctrl-v': 'vsplit',
  \ }

let $FZF_DEFAULT_OPTS = "--reverse --preview='head -50 {+}' --cycle --multi --bind 'btab:toggle-up,tab:toggle-down'"

" Customize fzf colors to match your color scheme
" - fzf#wrap translates this to a set of `--color` options
let g:fzf_colors =
\ { 'fg':      ['fg', 'Normal'],
  \ 'bg':      ['bg', 'Normal'],
  \ 'hl':      ['fg', 'Comment'],
  \ 'fg+':     ['fg', 'CursorLine', 'CursorColumn', 'Normal'],
  \ 'bg+':     ['bg', 'CursorLine', 'CursorColumn'],
  \ 'hl+':     ['fg', 'Statement'],
  \ 'info':    ['fg', 'PreProc'],
  \ 'border':  ['fg', 'Ignore'],
  \ 'prompt':  ['fg', 'Conditional'],
  \ 'pointer': ['fg', 'Exception'],
  \ 'marker':  ['fg', 'Keyword'],
  \ 'spinner': ['fg', 'Label'],
  \ 'header':  ['fg', 'Comment'] }

" Enable per-command history
" - History files will be stored in the specified directory
" - When set, CTRL-N and CTRL-P will be bound to 'next-history' and
"   'previous-history' instead of 'down' and 'up'.
let g:fzf_history_dir = '~/.local/share/fzf-history'

if exists('$TMUX')
  let g:fzf_layout = { 'window': { 'width': 0.9, 'height': 0.8 } }
else
  let g:fzf_layout = { 'window': { 'width': 0.9, 'height': 0.8 } }
endif

"------------------ Mapings
nnoremap <silent> <C-b>           :Buffers<cr>
nnoremap <silent> <C-p>          :Files<cr>
