scriptencoding utf-8
source ~/.config/nvim/settings/plugins.vim

set termguicolors
colorscheme gruvbox
set bg=dark

let g:gruvbox_dark_contrast="hard"

" set guicursor=
set bg=dark
set noerrorbells
set tabstop=4 softtabstop=4
set shiftwidth=4
set noswapfile
set mouse=a
set smartindent
set nobackup
set clipboard+=unnamedplus
set nu rnu
set hidden
set splitbelow
set splitright
set hls is ic

cnoreabbrev W! w!
cnoreabbrev Q! q!
cnoreabbrev Qall! qall!
cnoreabbrev Wq wq
cnoreabbrev Wa wa
cnoreabbrev wQ wq
cnoreabbrev WQ wq
cnoreabbrev W w
cnoreabbrev Q q
cnoreabbrev Qall qall


" hi Normal guibg=NONE ctermbg=NONE

""--------- Startup screen
source ~/.config/nvim/settings/start-screen.vim

"" -------- Load airline
source ~/.config/nvim/settings/airline.vim

""============ AutoComplete Configuration ============================"
source ~/.config/nvim/settings/autocomplete_nvim.vim

"""============================== FZF Configuration ==========================="
source ~/.config/nvim/settings/fzf.vim

"" -------------- Load all the mapings
source ~/.config/nvim/settings/mapings.vim
