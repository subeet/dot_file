lua << END
require'nvim_lsp'.clangd.setup{}
require'nvim_lsp'.tsserver.setup{}
require'nvim_lsp'.sumneko_lua.setup{}
require'nvim_lsp'.pyls_ms.setup{}
-- require'nvim_lsp'.pyls.setup{}
END


"----------------- Neovim - LSP --------------------""

nnoremap <silent> <leader>gd    <cmd>lua vim.lsp.buf.declaration()<CR>
nnoremap <silent> <c-]>         <cmd>lua vim.lsp.buf.definition()<CR>
nnoremap <silent> <leader>K     <cmd>lua vim.lsp.buf.hover()<CR>
nnoremap <silent> <leader>gD    <cmd>lua vim.lsp.buf.implementation()<CR>
nnoremap <silent> <c-k>         <cmd>lua vim.lsp.buf.signature_help()<CR>
nnoremap <silent> <leader>1gD   <cmd>lua vim.lsp.buf.type_definition()<CR>
nnoremap <silent> <leader>gr    <cmd>lua vim.lsp.buf.references()<CR>
nnoremap <silent> <leader>g0    <cmd>lua vim.lsp.buf.document_symbol()<CR>
nnoremap <silent> <leader>gW    <cmd>lua vim.lsp.buf.workspace_symbol()<CR>
nnoremap <silent> <leader>F     <cmd>lua vim.lsp.buf.formatting()<CR>
nnoremap <silent> <leader>fr    <cmd>lua vim.lsp.buf.rename()<CR>
