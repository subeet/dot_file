let mapleader=' '

nnoremap <leader>j <C-W><C-J>
nnoremap <leader>k <C-W><C-K>
nnoremap <leader>l <C-W><C-L>
nnoremap <leader>h <C-W><C-H>

nnoremap <leader>sfr :%s/
nnoremap <leader>/ :noh<CR>


" not losing visual mode after performing operation
vmap < <gv
vmap > >gv


" Changing current directory
nnoremap <leader>cd :cd %:p:h<CR>:pwd<CR>

"========== FZF Mapings ================"

map <c-p> :Files<CR>
map <leader>b :Buffers<CR>


"----- Git Mapings -------------

nnoremap <leader>gs   <cmd>G<CR>
nnoremap <leader>gp   <cmd>Gpush<CR>
nnoremap <leader>gP   <cmd>Gpull<CR>

"------------------------ Autocmd ------------------

" Delete white trailing spaces
autocmd BufWritePre * %s/\s\+$//e

" for compiling c++
autocmd filetype cpp map F9 :!st -e bash -c 'g++ -std=c++17 -Wshadow -Wall -g -o program %:r.cpp -O2 -Wno-unused-result -DLOCAL;read' <CR>
autocmd filetype cpp map <F8> :!g++ -std=c++17 -Wshadow -Wall -g -o program "%:r.cpp" -O2 -Wno-unused-result -DLOCAL<CR>
autocmd filetype cpp map <F9> :!st -e bash -c '"%:p:h/program" ; read'<CR>

" Run xrdb whenever Xdefaults or Xresources are updated.
autocmd BufWritePost *Xresources,*Xdefaults !xrdb %

" Update binds when sxhkdrc is updated.
autocmd BufWritePost *sxhkdrc !pkill -USR1 sxhkd

" Auto Format javascript file
autocmd BufWritePost *.js lua vim.lsp.buf.formatting()


autocmd filetype xdefaults ColorHighlight<CR>
