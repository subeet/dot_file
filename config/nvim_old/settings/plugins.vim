let vimplug_exists=expand('~/.config/nvim/autoload/plug.vim')
let g:vim_bootstrap_langs = "c,html,javascript,lua,typescript"
let g:vim_bootstrap_editor = "nvim"				" nvim or vim
if !filereadable(vimplug_exists)
  if !executable("curl")
    echoerr "You have to install curl or first install vim-plug yourself!"
    execute "q!"
  endif
  echo "Installing Vim-Plug..."
  echo ""
  silent exec "!\curl -fLo " . vimplug_exists . " --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim"
  let g:not_finish_vimplug = "yes"
  autocmd VimEnter * PlugInstall
endif


call plug#begin(expand('~/.config/nvim/plugged'))

"------- Auto completions

" Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
" Plug 'Shougo/deoplete-lsp'
" Plug 'neovim/nvim-lsp'
" Use release branch (recommend)
 Plug 'neoclide/coc.nvim', {'branch': 'release'}


"---- Project Managment
Plug 'junegunn/fzf'
Plug 'junegunn/fzf.vim'
Plug 'mhinz/vim-startify'
Plug 'preservim/nerdtree'


"-- git plugin
Plug 'tpope/vim-fugitive'
Plug 'airblade/vim-gitgutter'
Plug 'mhinz/vim-signify'


"---- utility plugins
Plug 'tpope/vim-commentary'
Plug 'christoomey/vim-tmux-navigator'
Plug 'tpope/vim-surround'


"--- enhanced programing
Plug 'sheerun/vim-polyglot'
Plug 'honza/vim-snippets'
Plug 'ThePrimeagen/vim-be-good', {'do': './install.sh'}


" =============== Misc Plugins ======================="
Plug 'mboughaba/i3config.vim'
Plug 'chrisbra/Colorizer'
Plug 'ThePrimeagen/vim-be-good', {'do': './install.sh'}


"================ Color Schemes ======================="
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'rafi/awesome-vim-colorschemes'


call plug#end()
