" better key bindings for UltiSnipsExpandTrigger
"let g:UltiSnipsExpandTrigger="<cr>"
let g:UltiSnipsJumpForwardTrigger="<c-j>"
let g:UltiSnipsJumpBackwardTrigger="<c-k>"

" configure lsp
"require'nvim_lsp'.rust_analyzer.setup{on_attach=require'diagnostic'.on_attach}
lua << EOF
  require'nvim_lsp'.gopls.setup{}
  require'nvim_lsp'.rust_analyzer.setup{}
  require'nvim_lsp'.tsserver.setup{}
  require'nvim_lsp'.pyls.setup{}
  require'nvim_lsp'.cssls.setup{}
  require'nvim_lsp'.bashls.setup{}
  require'nvim_lsp'.sumneko_lua.setup{}
  require'nvim_lsp'.clangd.setup{}
EOF
autocmd BufEnter * lua require'completion'.on_attach()
autocmd Filetype cpp,go,python,ts,typescript,rust,javascript setlocal omnifunc=v:lua.vim.lsp.omnifunc

let g:completion_enable_auto_popup = 1
function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~ '\s'
endfunction

inoremap <silent><expr> <TAB>
  \ pumvisible() ? "\<C-n>" :
  \ <SID>check_back_space() ? "\<TAB>" :
  \ completion#trigger_completion()

" possible value: 'UltiSnips', 'Neosnippet', 'vim-vsnip'
let g:completion_enable_snippet = 'UltiSnips'

" Use <Tab> and <S-Tab> to navigate through popup menu
inoremap <expr> <Tab>   pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"

" Set completeopt to have a better completion experience
set completeopt=menuone,noinsert,noselect

" Avoid showing message extra message when using completion
set shortmess+=c

" for better error viewing
set signcolumn=yes

" Setup default LSP keybinds
nnoremap <silent> gd    <cmd>lua vim.lsp.buf.declaration()<CR>
nnoremap <silent> <c-]> <cmd>lua vim.lsp.buf.definition()<CR>
nnoremap <silent> K     <cmd>lua vim.lsp.buf.hover()<CR>
nnoremap <silent> gr    <cmd>lua vim.lsp.buf.references()<CR>
nnoremap <silent> g0    <cmd>lua vim.lsp.buf.document_symbol()<CR>
nnoremap <silent> gW    <cmd>lua vim.lsp.buf.workspace_symbol()<CR>
