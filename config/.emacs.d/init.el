(require 'package)

(add-to-list 'package-archives '("org" . "https://orgmode.org/elpa/") t)
(add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/"))
(add-to-list 'package-archives '("melpa-stable" . "http://stable.melpa.org/packages/"))

(setq package-enable-at-startup nil)
(package-initialize)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(company-quickhelp-color-background "#D0D0D0")
 '(company-quickhelp-color-foreground "#494B53")
 '(custom-enabled-themes (quote (leuven)))
 '(custom-safe-themes
   (quote
    ("0dd2666921bd4c651c7f8a724b3416e95228a13fca1aa27dc0022f4e023bf197" "a06658a45f043cd95549d6845454ad1c1d6e24a99271676ae56157619952394a" default)))
 '(package-selected-packages
   (quote
    (powerline lua-mode flymake-lua one-themes irony-eldoc company-irony which-key iedit ace-window aggressive-indent hungry-delete use-package))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(lsp-ui-doc-background ((t (:background nil))))
 '(lsp-ui-doc-header ((t (:inherit (font-lock-string-face italic))))))

;; for Use-package
(eval-when-compile
  (require 'use-package))


; mmmmm           m                    m""
;   #    m mm   mm#mm   mmm    m mm  mm#mm   mmm    mmm    mmm
;   #    #"  #    #    #"  #   #"  "   #    "   #  #"  "  #"  #
;   #    #   #    #    #""""   #       #    m"""#  #      #""""
; mm#mm  #   #    "mm  "#mm"   #       #    "mm"#  "#mm"  "#mm"



;mmmmmmm                      #
;   #   m     m  mmm    mmm   #   m   mmm
;   #   "m m m" #"  #  "   #  # m"   #   "
;   #    #m#m#  #""""  m"""#  #"#     """m
;   #     # #   "#mm"  "mm"#  #  "m  "mmm"

(setq inhibit-startup-message t)
(tool-bar-mode -1)
(fset 'yes-or-no-p 'y-or-n-p)

;; set tab-width to 4
(setq tab-stop-list (number-sequence 4 200 4))
(setq-default indent-tabs-mode nil)
(setq-default tab-width 4)
;; (setq indent-line-function 'insert-tab)

(use-package evil
  :config
  (evil-mode t))
;; (use-package parchment-theme :ensure t)
;; (use-package cloud-theme :ensure t)
;; (use-package moe-theme :ensure t)
;; (use-package zenburn-theme :ensure t)
;; (use-package monokai-theme :ensure t)
;; (use-package gruvbox-theme :ensure t)
;; (use-package ample-theme :ensure t)
;; (use-package ample-zen-theme :ensure t)
;; (use-package alect-themes :ensure t)
;; (use-package tao-theme :ensure t)
;; (use-package poet-theme :ensure t)
;; (use-package modus-operandi-theme :ensure t)
;; (use-package modus-vivendi-theme :ensure t)
;; (use-package faff-theme :ensure t)
;; (use-package color-theme-modern :ensure t)

;; (use-package gruox-theme
;;   :init
;;   (load-theme 'ad t))

(use-package powerline
  :ensure t
  :config
  (progn
    (powerline-center-evil-theme)
    )
  )
(use-package doom-modeline
  :ensure t)
(doom-modeline-mode)

(defun xah-comment-dwim ()
  "Like `comment-dwim', but toggle comment if cursor is not at end of line.

URL `http://ergoemacs.org/emacs/emacs_toggle_comment_by_line.html'
Version 2016-10-25"
  (interactive)
  (if (region-active-p)
      (comment-dwim nil)
    (let (($lbp (line-beginning-position))
          ($lep (line-end-position)))
      (if (eq $lbp $lep)
          (progn
            (comment-dwim nil))
        (if (eq (point) $lep)
            (progn
              (comment-dwim nil))
          (progn
            (comment-or-uncomment-region $lbp $lep)
            (forward-line )))))))

(define-key evil-visual-state-map (kbd "gcc") 'xah-comment-dwim)

;; Delete all the whitespaces
(use-package hungry-delete
  :ensure t
  :config
  (global-hungry-delete-mode))

;; (global-auto-revert-mode 1) ;; you might not want this
;; (setq auto-revert-verbose nil) ;; or this
;; (global-set-key (kbd "<f5>") 'revert-buffer)

;; Smart Indent
(use-package aggressive-indent
  :ensure t
  :config
  (global-aggressive-indent-mode 1)
  ;;(add-to-list 'aggressive-indent-excluded-modes 'html-mode)
  )

;; Window managment
(use-package ace-window
  :ensure t
  :config
  (setq aw-scope 'frame) ;; was global
  (global-set-key (kbd "M-o") 'other-frame)
  (global-set-key [remap other-window] 'ace-window))

;; (use-package try :ensure t)
;; (use-package posframe :ensure t)
(use-package iedit
:ensure t)

(use-package which-key
    :ensure t
    :config
    (which-key-mode))

;; (use-package pcre2el
;;     :ensure t
;;     :config
;;     (pcre-mode)
;;     )

;; (add-hook 'org-mode-hook 'turn-on-flyspell)
;; (add-hook 'org-mode-hook 'turn-on-auto-fill)
;; (add-hook 'mu4e-compose-mode-hook 'turn-on-flyspell)
;; (add-hook 'mu4e-compose-mode-hook 'turn-on-auto-fill)


 ;;  mmmm           "                                mmmmm
 ;; #"   "m     m mmm    mmmm    mmm    m mm           #    m   m  m   m
 ;; "#mmm "m m m"   #    #" "#  #"  #   #"  "          #    "m m"  "m m"
 ;;     "# #m#m#    #    #   #  #""""   #              #     #m#    #m#
 ;; "mmm#"  # #   mm#mm  ##m#"  "#mm"   #       #    mm#mm    #     "#
 ;;                      #                     "                    m"
 ;;                      "                                         ""

 ;;                   #           mmm                                     ""#
 ;;  mmm   m mm    mmm#         m"   "  mmm   m   m  m mm    mmm    mmm     #
 ;; "   #  #"  #  #" "#         #      #" "#  #   #  #"  #  #   "  #"  #    #
 ;; m"""#  #   #  #   #         #      #   #  #   #  #   #   """m  #""""    #
 ;; "mm"#  #   #  "#m##          "mmm" "#m#"  "mm"#  #   #  "mmm"  "#mm"    "mm

(use-package counsel
  :ensure t
  :config
  (progn
    (define-key evil-normal-state-map (kbd "SPC f") 'counsel-fzf)
    )

(use-package ivy
  :ensure t
  :diminish (ivy-mode)
  :config
  (progn
    (ivy-mode 1)
    (setq ivy-use-virtual-buffers t)
    (setq ivy-count-format "%d/%d ")
    (setq ivy-display-style 'fancy))
    (define-key evil-normal-state-map (kbd "SPC b") 'ivy-switch-buffer)
    ))


(use-package swiper
  :ensure t
  :config
  (progn
    (ivy-mode 1)
    (setq ivy-use-virtual-buffers t)
    (setq ivy-display-style 'fancy)
    ;; (define-key read-expression-map (kbd "C-r") 'counsel-expression-history)
    (define-key evil-normal-state-map (kbd "/") 'swiper-isearch)
	;; ("C-c C-r" . ivy-resume)
    (define-key evil-normal-state-map (kbd "M-x") 'counsel-M-x)
    (define-key evil-normal-state-map (kbd "SPC .") 'counsel-find-file)
    ))

;; Vim easyMotion
(use-package avy
  :ensure t
  :config
  (progn
    (define-key evil-normal-state-map (kbd "SPC /") 'avy-goto-char)
    (define-key evil-insert-state-map (kbd "M-s") 'avy-goto-char))
  )

;; LSP CONFIGURATION
(use-package lsp-mode
  :ensure t
  :commands lsp
  :custom
  (lsp-auto-guess-root nil)
  :bind (:map lsp-mode-map ("C-c C-f" . lsp-format-buffer))
  :hook ((python-mode c-mode c++-mode) . lsp)
  )
(use-package lsp-ui
  :after lsp-mode
  :diminish
  :commands lsp-ui-mode
  :custom-face
  (lsp-ui-doc-background ((t (:background nil))))
  (lsp-ui-doc-header ((t (:inherit (font-lock-string-face italic)))))
  :bind (:map lsp-ui-mode-map
              ([remap xref-find-definitions] . lsp-ui-peek-find-definitions)
              ([remap xref-find-references] . lsp-ui-peek-find-references)
              ("C-c u" . lsp-ui-imenu))
  :custom
  (lsp-ui-doc-enable t)
  (lsp-ui-doc-header t)
  (lsp-ui-doc-include-signature t)
  (lsp-ui-doc-position 'top)
  (lsp-ui-doc-border (face-foreground 'default))
  (lsp-ui-sideline-enable nil)
  (lsp-ui-sideline-ignore-duplicate t)
  (lsp-ui-sideline-show-code-actions nil)
  :config
  ;; Use lsp-ui-doc-webkit only in GUI
  ;; WORKAROUND Hide mode-line of the lsp-ui-imenu buffer
  ;; https://github.com/emacs-lsp/lsp-ui/issues/243
  (defadvice lsp-ui-imenu (after hide-lsp-ui-imenu-mode-line activate)
    (setq mode-line-format nil)))

(use-package company
  :ensure t
  :config
  (progn
    (setq company-idle-delay 0)
    (setq company-minimum-prefix-length 3)
    (global-company-mode t)
    )
  )

(use-package company-lsp
  :ensure t
  :config
  (progn
    ;; (setq company-lsp-enable-snippet t)
    (push 'company-lsp company-backends)
    )
  )

;; BUFFER
;; (global-set-key (kbd "C-x C-b") 'ibuffer)
;; (setq ibuffer-saved-filter-groups
;;       (quote (("default"
;;                ("dired" (mode . dired-mode))
;;                ("org" (name . "^.*org$"))
;;                ("magit" (mode . magit-mode))
;;                ("IRC" (or (mode . circe-channel-mode) (mode . circe-server-mode)))
;;                ("web" (or (mode . web-mode) (mode . js2-mode)))
;;                ("shell" (or (mode . eshell-mode) (mode . shell-mode)))
;;                ("mu4e" (or

;;                         (mode . mu4e-compose-mode)
;;                         (name . "\*mu4e\*")
;;                         ))
;;                ("programming" (or
;;                                (mode . clojure-mode)
;;                                (mode . clojurescript-mode)
;;                                (mode . python-mode)
;;                                (mode . c++-mode)))
;;                ("emacs" (or
;;                          (name . "^\\*scratch\\*$")
;;                          (name . "^\\*Messages\\*$")))
;;                ))))
;; (add-hook 'ibuffer-mode-hook
;;           (lambda ()
;;             (ibuffer-auto-mode 1)
;;             (ibuffer-switch-to-saved-filter-groups "default")))

;; ;; don't show these
;;                                         ;(add-to-list 'ibuffer-never-show-predicates "zowie")
;; ;; Don't show filter groups if there are no buffers in that group
;; (setq ibuffer-show-empty-filter-groups nil)

;; ;; Don't ask for confirmation to delete marked buffers
;; (setq ibuffer-expert t)


;; FLYCHECK
(use-package flycheck
  :ensure t
  :init
  (global-flycheck-mode t))

;; PROJECT MANAGMENT PROJECTILE
;;  (use-package projectile
;;     :ensure t
;;     :bind (:map projectile-mode-map
;;                   ("s-p" . 'projectile-command-map)
;;                   ("C-c p" . 'projectile-command-map)
;;                 )

;;     :config
;;     (setq projectile-completion-system 'ivy)

;;     (projectile-mode +1))


;; (use-package ibuffer-projectile
;; :ensure t
;; :config
;; (add-hook 'ibuffer-hook
;;     (lambda ()
;;       (ibuffer-projectile-set-filter-groups)
;;       (unless (eq ibuffer-sorting-mode 'alphabetic)
;;         (ibuffer-do-sort-by-alphabetic))))


;; PYTHON CONFIGURATION
;; (use-package virtualenvwrapper
;;   :ensure t
;;   :config
;;   (progn
;;     (venv-initialize-interactive-shells)
;;     (venv-initialize-eshell))
;;   )

;; (venv-workon "p3")
;; (setq lsp-python-executable-cmd "python3")

;; (setq python-shell-interpreter "python3"
;;       python-shell-interpreter-args "-i")

(use-package company-irony
    :ensure t
    :config
    (add-to-list 'company-backends 'company-irony)
    )

(use-package irony
    :ensure t
    :config
    (add-hook 'c++-mode-hook 'irony-mode)
    (add-hook 'c-mode-hook 'irony-mode)
    (add-hook 'irony-mode-hook 'irony-cdb-autosetup-compile-options)
    )

(use-package irony-eldoc
  :ensure t
  :config
  (add-hook 'irony-mode-hook #'irony-eldoc))



;; (setq lsp-clients-clangd-executable )
;; (setq lsp-clangd-executable "clangd")
(setq lsp-clients-clangd-executable "clangd")

(linum-mode t)
