(define-package "one-themes" "20200704.1115" "One Colorscheme"
  '((emacs "24"))
  :commit "664ad1e0db480210aaab8ebde6938865b86e6146" :authors
  '(("Balaji Sivaraman" . "balaji@balajisivaraman.com"))
  :maintainer
  '("Balaji Sivaraman" . "balaji@balajisivaraman.com")
  :url "http://github.com/balajisivaraman/emacs-one-themes")
;; Local Variables:
;; no-byte-compile: t
;; End:
