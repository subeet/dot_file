;;; -*- no-byte-compile: t -*-
(define-package "parchment-theme" "20200514.2047" "Light theme inspired by Acme and Leuven" '((autothemer "0.2")) :commit "eff75ab5a6ec28e8280aae3f1e4c50fdb71e976a" :authors '(("Alex Griffin" . "a@ajgrf.com")) :maintainer '("Alex Griffin" . "a@ajgrf.com") :url "https://github.com/ajgrf/parchment")
