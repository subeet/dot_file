(define-package "iedit" "20200705.1459" "Edit multiple regions in the same way simultaneously." 'nil :commit "0094a674e396909ceea486e7cdbd014de3509ea7" :keywords
  '("occurrence" "region" "simultaneous" "refactoring")
  :authors
  '(("Victor Ren" . "victorhge@gmail.com"))
  :maintainer
  '("Victor Ren" . "victorhge@gmail.com")
  :url "https://www.emacswiki.org/emacs/Iedit")
;; Local Variables:
;; no-byte-compile: t
;; End:
