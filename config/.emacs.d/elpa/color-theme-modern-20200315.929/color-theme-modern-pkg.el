(define-package "color-theme-modern" "20200315.929" "Reimplement colortheme with Emacs 24 theme framework."
  '((emacs "24"))
  :commit "40464198e7bf2121694a7e6d87588342140a84ff" :url "https://github.com/emacs-jp/replace-colorthemes/")
;; Local Variables:
;; no-byte-compile: t
;; End:
