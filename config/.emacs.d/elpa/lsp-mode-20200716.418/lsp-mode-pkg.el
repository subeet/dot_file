(define-package "lsp-mode" "20200716.418" "LSP mode"
  '((emacs "25.1")
    (dash "2.14.1")
    (dash-functional "2.14.1")
    (f "0.20.0")
    (ht "2.0")
    (spinner "1.7.3")
    (markdown-mode "2.3")
    (lv "0"))
  :commit "0a2993eca0bfc3c5ee481dc38b84fab7d7878c61" :keywords
  '("languages")
  :authors
  '(("Vibhav Pant, Fangrui Song, Ivan Yonchovski"))
  :maintainer
  '("Vibhav Pant, Fangrui Song, Ivan Yonchovski")
  :url "https://github.com/emacs-lsp/lsp-mode")
;; Local Variables:
;; no-byte-compile: t
;; End:
