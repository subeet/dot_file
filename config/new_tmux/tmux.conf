# vi:filetype=tmux

#unbind defautl prefix
set -g prefix C-Space
setw -g mode-keys vi
setw -g mode-style bg=black

set -sg escape-time 0
set-option -g default-terminal "xterm-256color"
set-option -g pane-active-border-style fg=green
set-window-option -g xterm-keys on # for vim
set-window-option -g mode-keys vi # vi key
set-window-option -g monitor-activity on
set-window-option -g window-status-current-style fg=white
setw -g window-status-current-style reverse
setw -g automatic-rename
set -g mouse on
set -g history-limit 30000
set -g terminal-overrides 'xterm*:smcup@:rmcup@'
set-option -g status-justify right
set-option -g status-bg colour238  # colour213 # pink
set-option -g status-fg cyan
set-option -g status-interval 5
set-option -g status-left-length 30
set-option -g status-left '#[fg=magenta]» #[fg=blue,bold]#T#[default]  #[fg=cyan]»» #[fg=blue,bold]###S #[fg=magenta]%R %m-%d#(acpi | cut -d ',' -f 2)#[default]'
set-option -g status-right '#[fg=red,bold][[ #(git branch) branch ]] '
set-option -g visual-activity on
set-option -g set-titles on
set-option -g set-titles-string '#H:#S.#I.#P #W #T'

set-window-option -g window-status-current-style bg=red

unbind C-b
unbind '"'
unbind %
unbind j

# Source tmux config
bind-key r source-file ~/.config/tmux/tmux.conf

# Opening of new window
bind = new-window
bind C-j previous-window
bind C-k next-window
bind ^space next-window

bind Space list-panes
bind Enter break-pane

# Spliting window
bind \\ split-window -v
bind - split-window -h

bind A command-prompt "rename-window %%"
setw -g aggressive-resize on

bind-key h select-pane -L
bind-key j select-pane -D
bind-key k select-pane -U
bind-key l select-pane -R

bind o select-layout "active-only"
bind M-- select-layout "even-vertical"
bind M-\\ select-layout "even-horizontal"
bind M-r rotate-window

# Resizing (mouse also works).
unbind Left
bind -r Left resize-pane -L 5
unbind Right
bind -r Right resize-pane -R 5
unbind Down
bind -r Down resize-pane -D 5
unbind Up
bind -r Up resize-pane -U 5

# Tmux Ressurect
run-shell $HOME/.config/tmux/tmux-resurrect/resurrect.tmux
