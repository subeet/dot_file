const int N = 5e5;
class SegmentTree{
    public:
        int tree[4*N];

        void build(int node,int L,int R,int arr[]){
            if(L == R){
                tree[node] = arr[L];
                return;
            }
            int m = (L+R)/2;
            build(2*node,L,m,arr);
            build(2*node+1,m+1,R,arr);
            tree[node] = tree[2*node] + tree[2*node+1];
        }

        void update(int node,int L,int R,int idx,int val){
            if(L == R){
                tree[node] = val;
                return;
            }
            int m = (L+R)/2;
            if(idx <= m) update(2*node,L,m,idx,val);
            else update(2*node+1,m+1,R,idx,val);
            tree[node] = tree[2*node] + tree[2*node+1];
        }

        int querry(int node,int L,int R,int l,int r){
            if(R<l || L > r) return 0;
            if( L >= l && R <= r) return tree[node];
            int mid = (L+R)/2;
            int p1 = querry(2*node,L,mid,l,r);
            int p2 = querry(2*node+1,mid+1,R,l,r);
            return p1+p2;
        }
}
