// 😀 😀 😀 😀 😀 😀 😀 
const int word_size = 26;
class Node{
public:
    Node* nxt[word_size];
    int cnt;
    bool isEnd;

    Node(){
        for(int i=0;i<word_size;i++){
            this->nxt[i] = nullptr;
        }
        this->cnt = 0;
        isEnd = false;
    }
};
class Trie{
public:

    Node* head;
    
    Trie(){
        this->head = new Node();
    }

    void insert(int x){
        Node* cur = head;
        for(int i = 30 ; i >= 0 ; i-- ){
            int b = (x>>i)&1;
            if(!cur->nxt[b]){
                cur->nxt[b] = new Node();
            }
            cur = cur->nxt[b];
            cur->cnt++;
        }
        cur->isEnd = true;
    }

    void remove(int x){
        Node* cur = head;
        for(int i=30;i>=0;i--){
            int b = (x>>i)&i;
            cur=cur->nxt[b];
            cur->cnt--;
        }
    }

    int fun(int x){
        // example of xor
        Node* cur = head;
        int ans = 0;
        for(int i=30;i>=0;i--){
            int b = (x>>i)&1;
            if(cur->nxt[!b] && cur->nxt[!b]->cnt > 0){
                ans += (1ll<<i);
                cur = cur->nxt[!b];
            }else{
                cur = cur->nxt[b];
            }
        }
        return ans;
    }
};
