;;; Emacs Configurations                              -*- no-byte-compile: t -*-

(let ((min-version "26.0"))
  (if (version< emacs-version min-version)
      (error "Emacs v. %s+ is required for this configuration!" min-version)))

;; Constants.
;(defconst --emacs-start-time (current-time))
;(defconst --lisp-dir (concat user-emacs-directory "lisp/"))
;(defconst --misc-dir (concat user-emacs-directory "misc/"))
;(defconst --yas-dir (concat user-emacs-directory "snippets/"))
;(defconst --themes-dir (concat user-emacs-directory "themes/"))
;(defconst --user-cache-dir (concat user-emacs-directory "cache/"))
;(defconst --auto-save-dir (concat user-emacs-directory "auto-save/"))

;;; Set to `t' to enable config loading benchmarking and showing results when finished.
;(defconst --do-init-benchmark nil)

;(setq custom-theme-directory --themes-dir)

;;; Set the random number seed from the system's entropy pool.
;(random t)

;;; Prefer newest version of a file. This is especially useful for compiled files.
;(setq load-prefer-newer t)

;;; Create necessary directories if missing.
;(mkdir --user-cache-dir)
;(mkdir --auto-save-dir)

;; Speed up loading by removing handlers until finished. It contains a lot of regexps for matching
;; handlers to file names but it is not necessary while loading.
;(setq file-name-handler-alist-old file-name-handler-alist
      ;file-name-handler-alist nil)

;;; Speed up loading by disabling garbage collection until finished.
;(setq gc-cons-threshold most-positive-fixnum)

(require 'package)
(add-to-list 'package-archives '("org" . "https://orgmode.org/elpa/") t)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
(add-to-list 'package-archives '("melpa-stable" . "https://stable.melpa.org/packages/"))

(setq package-enabl-at-startup nil)
(package-initialize)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
    '(package-selected-packages
	 (quote
	     (helm-lsp company-lsp company lsp-ui lsp-mode async popup hydra flycheck avy rainbow-delimiters counsel which-key evil-leader doom-modeline undo-tree use-package))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

(eval-when-compile
    (require 'use-package))

(use-package evil
    :ensure t
    :config
    (evil-mode t))

;; User-Interface
(setq-default lisp-indent-offset 4)
(setq-default c-basic-offset 4)

;; Very Basic
(setq-default buffer-file-coding-system 'utf-8-unix)
(set-default-coding-systems 'utf-8-unix)
(setq locale-coding-system 'utf-8-unix)
(prefer-coding-system 'utf-8-unix)
(setq-default global-display-line-numbers-mode t)

;; Backup Directory
(setq backup-directory-alist '(("." . "~/.cache/saves")))
(setq backup-by-copying t)
(setq delete-old-versions t
      kept-new-versions 6
      kept-old-versions 3
      version-control t)
;if You don't want to have backup files
;(setq makeup-backup-file nil)

(setq inhibit-startup-message t)
(fset 'yes-or-no-p 'y-or-n-p)

(load-theme 'adwaita t)
(use-package doom-modeline
    :ensure t
    :init (doom-modeline-mode 1))

;; Vim key bindings
(use-package evil-leader
    :ensure t
    :config
    (progn
	(evil-leader/set-leader "<SPC>")
	(global-evil-leader-mode)
	(evil-leader/set-key
	    "c i" 'comment-or-uncomment-region)
	    ;; "cl" 'evilnc-quick-comment-or-uncomment-to-the-line
	    ;; "ll" 'evilnc-quick-comment-or-uncomment-to-the-line
	    ;; "cc" 'evilnc-copy-and-comment-lines
	    ;; "cp" 'evilnc-comment-or-uncomment-paragraphs
	    ;; "cr" 'comment-or-uncomment-region
	    ;; "cv" 'evilnc-toggle-invert-comment-line-by-line
	    ;; "\\" 'evilnc-comment-operator
	)
    )

(use-package which-key
    :ensure t
    :config
    (which-key-mode))


;; Counsel
(use-package counsel
    :ensure t
    :config
    (progn
	(evil-leader/set-key "f" 'counsel-find-file)
	)
    )


;; Ivy
(use-package ivy
    :ensure t
    :diminish (ivy-mode)
    :config
    (progn
	(ivy-mode 1)
	(setq ivy-use-virtual-buffers t)
	(setq ivy-count-format "%d%d ")
	(setq ivy-display-style 'fancy)
	(evil-leader/set-key "b" 'ivy-switch-buffer)
	(define-key evil-normal-state-map (kbd "/") 'swiper-isearch)
	(evil-leader/set-key "x" 'counsel-M-x)
	(evil-leader/set-key "." 'counsel-find-file)
	(evil-leader/set-key "h f" 'counsel-describe-function )
	(evil-leader/set-key "h v" 'counsel-describe-variable )
	(evil-leader/set-key "g" 'counsel-git )
	(evil-leader/set-key "k" 'counsel-rg )
	(setq ivy-re-builders-alist
	    '((t . ivy--regex-fuzzy)))
	)
    )

;; Rainbow parenthesis
(use-package rainbow-delimiters
    :ensure t
    :config
    (add-hook 'prog-mode-hook 'rainbow-delimiters-mode)
    )


;; Vim EasyMotion
(use-package avy
    :ensure t
    :config
    (progn
	(evil-leader/set-key "/ c" 'avy-goto-char)
	)
    )

;; Language Server Configuration

;;LSP
(use-package lsp-mode
    :ensure t
    :config
    (add-hook 'c++-mode-hook #'lsp)
    )

;(use-package lsp-mode
    ;:ensure t
    ;:hook(
		 ;(prog-lisp-mode . lsp)
		 ;(lsp-mode . lsp-enable-which-key-integration))
    ;:commands lsp
    ;)

;(add-hook 'c++-mode-hook' #'lsp)
;(add-hook 'python-mode-hook' #'lsp)
;(add-hook 'c-mode-hook' #'lsp)
;(add-hook 'rust-mode-hook' #'lsp)
;(setq lsp-clients-clangd-args '("-j=4" "-background-index" "-log=error"))

;; FlyCheck for error
;; (use-package flycheck
;;     :ensure t
;;     :init (global-flycheck-mode)
;;     :commands flycheck-mode
;;     )

;; (use-package hydra
;;     :ensure t)

;; (use-package popup
;;     :ensure t)
;; (use-package async
;;     :ensure t)
;; ;; (use-package helm) will be insyalled while installing help-lsp

;; (use-package helm-lsp
;;   :config
;;   (defun netrom/helm-lsp-workspace-symbol-at-point ()
;;     (interactive)
;;     (let ((current-prefix-arg t))
;;       (call-interactively #'helm-lsp-workspace-symbol)))

;;   (defun netrom/helm-lsp-global-workspace-symbol-at-point ()
;;     (interactive)
;;     (let ((current-prefix-arg t))
;;       (call-interactively #'helm-lsp-global-workspace-symbol))))

;; (use-package lsp-mode
;;     :ensure t
;;     :requires (hydra helm helm-lsp)
;;     :config
;;     (progn
;;   (setq netrom--general-lsp-hydra-heads
;;         '(;; Xref
;;           ("d" xref-find-definitions "Definitions" :column "Xref")
;;           ("D" xref-find-definitions-other-window "-> other win")
;;           ("r" xref-find-references "References")
;;           ("s" netrom/helm-lsp-workspace-symbol-at-point "Helm search")
;;           ("S" netrom/helm-lsp-global-workspace-symbol-at-point "Helm global search")

;;           ;; Peek
;;           ("C-d" lsp-ui-peek-find-definitions "Definitions" :column "Peek")
;;           ("C-r" lsp-ui-peek-find-references "References")
;;           ("C-i" lsp-ui-peek-find-implementation "Implementation")

;;           ;; LSP
;;           ("p" lsp-describe-thing-at-point "Describe at point" :column "LSP")
;;           ("C-a" lsp-execute-code-action "Execute code action")
;;           ("R" lsp-rename "Rename")
;;           ("t" lsp-goto-type-definition "Type definition")
;;           ("i" lsp-goto-implementation "Implementation")
;;           ("f" helm-imenu "Filter funcs/classes (Helm)")
;;           ("C-c" lsp-describe-session "Describe session")

;;           ;; Flycheck
;;           ("l" lsp-ui-flycheck-list "List errs/warns/notes" :column "Flycheck"))

;;         netrom--misc-lsp-hydra-heads
;;         '(;; Misc
;;           ("q" nil "Cancel" :column "Misc")
;;           ("b" pop-tag-mark "Back")))

;;   ;; Create general hydra.
;;   (eval `(defhydra netrom/lsp-hydra (:color blue :hint nil)
;;            ,@(append
;;               netrom--general-lsp-hydra-heads
;;               netrom--misc-lsp-hydra-heads)))

;;   (add-hook 'lsp-mode-hook
;;             (lambda () (local-set-key (kbd "C-c C-l") 'netrom/lsp-hydra/body)))))


;; ;; LSP-UI
;; (use-package lsp-ui
;;     :ensure t
;;     :requires lsp-mode flycheck
;;     :config
;;     (progn
;; 	(setq lsp-ui-doc-enable t)
;; 	(setq lsp-ui-doc-use-childframe t)
;; 	(setq   lsp-ui-doc-position 'top)
;; 	(setq   lsp-ui-doc-include-signature t)
;; 	(setq    lsp-ui-sideline-enable nil)
;; 	(setq    lsp-ui-flycheck-enable t)
;; 	(setq    lsp-ui-flycheck-list-position 'right)
;; 	(setq    lsp-ui-flycheck-live-reporting t)
;; 	(setq    lsp-ui-peek-enable t)
;; 	(setq    lsp-ui-peek-list-width 60)
;; 	(setq    lsp-ui-peek-peek-height 25)
;; 	(add-hook 'lsp-mode-hook' 'lsp-ui-mode)
;; 	)
;;     )

;; (add-hook 'c++-mode-hook' #'lsp)
;; (add-hook 'python-mode-hook' #'lsp)
;; (add-hook 'c-mode-hook' #'lsp)
;; (add-hook 'rust-mode-hook' #'lsp)
;; (setq lsp-clients-clangd-args '("-j=4" "-background-index" "-log=error"))

;; (use-package company
;;     :ensure t
;;   :config
;;   (setq company-idle-delay 0.2)

;;   (global-company-mode 1)

;;   (global-set-key (kbd "C-<tab>") 'company-complete))

;; (use-package company-lsp
;;   :requires (company)
;;     :ensure t
;;   :config
;;     (push 'company-lsp company-backends)

;;     ;; Disable client-side cache because the LSP server does a better job.
;;     (setq company-transformers nil
;; 	    company-lsp-async t
;; 	    company-lsp-cache-candidates nil))
